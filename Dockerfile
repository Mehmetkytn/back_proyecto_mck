# Imagen raiz
FROM node

# carpeta raiz
WORKDIR /bankam

# Copia de archivos
ADD . /bankam

# Añadir volumen
# VOLUME ['/logs'] 

# Exponer puerto
EXPOSE 3000

# Instalar dependencias
# RUN npm install

# Comando de inicializacion
CMD ["npm", "start"]
