var express = require ('express');
var app = express();
var bodyParser = require ('body-parser');
app.use(bodyParser.json());
var port = process.env.PORT || 3000;

var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechumck/collections/";
var mLabAPIKey = "apiKey=dFaKqFFgkCWcXcU9S5mkwWz8VU14oPAv";
var requestJson = require('request-json');

app.listen(port);
console.log("API escuchando en el puerto" + port);

var fecha = new Date();
var aa = fecha.getFullYear();
var mm = fecha.getMonth()+1;
var dd = fecha.getDate();

var mes = (mm < 10) ? '0' + mm : mm;
var dia = (dd < 10) ? '0' + dd : dd;

var hoyfecha = dia+"/"+mes+"/"+aa;


app.get ('/apitechu/v2/users',
  function (req,res){
    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("user?" + mLabAPIKey,
      function(err, resMlab, body){
        var response = !err ? body : {
          "msg" : "Error obteniendo usuarios"
        }
        res.send(response);
      }
    )
  }
);

app.get ('/apitechu/v2/users/:id',
  function (req,res){

    var id = req.params.id;
    var query = 'q={"id" : '+ id +'}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
        if (err) {
           response = {
            "msg" : "Error obteniendo usuario."
          }
            res.status(500);
        }else{
           if (body.length > 0) {
               response = body[0];
        }else{
           response = {
             "msg" : "Usuario no encontrado."
         };
           res.status(404);
        }
       }
          res.send(response);
      }
     )
   }
 );

app.get ('/apitechu/v2/users/accounts/:id',
  function (req,res){

    var userid = req.params.id;
    var query = 'q={"userid" : '+ userid +'}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo Cuenta."
         }
          res.status(500);
       }else{
           if (body.length > 0) {
            response = body;
       }else{
          response = {
            "msg" : "Cuenta no encontrado."
        };
          res.status(404);
        }
       }
          res.send(response);
      }
    )
   }
 );

app.post ("/apitechu/v2/login",
  function(req,res){

    var email = req.body.email
    var password = req.body.password
    var query = 'q={"email": "'+ email +'","password" :"'+ password +'"}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("user?" + query + "&" + mLabAPIKey,

      function(err, resMLab, body) {
        if (err) {
          response = {
            "msg" : "Error obteniendo usuario."
      }
        res.status(500);
      }else{
        if (body.length > 0) {
          var queryPut='q={"id":'+body[0].id+'}';
          var putBody ='{"$set":{"logged":true}}';
          response = body[0];

          httpClient.put("user?"+queryPut+'&'+mLabAPIKey, JSON.parse(putBody),
            function(errPut,resMlabPut,bodyPut){

              res.send(response);
            }
          );
      }else{
        response = {
          "msg" : "Usuario no encontrado."
          };
          res.status(404);
          res.send(response);
          }
        }
      }
    );
  }
);

app.post ("/apitechu/v2/logout",
  function(req,res){

    var email = req.body.email
    var query = 'q={"email": "'+ email +'","logged" :true}';

    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("user?" + query + "&" + mLabAPIKey,

    function(err, resMLab, body) {
      if (err) {
        response = {
         "msg" : "Error obteniendo usuario."
        }
         res.status(500);
      }else{
       if (body.length > 0) {
       var queryPut='q={"id":'+body[0].id+'}';
       var putBody ='{"$set":{"logged":""}}';
       response = body[0];

          httpClient.put("user?"+queryPut+'&'+mLabAPIKey, JSON.parse(putBody),
            function(errPut,resMlabPut,bodyPut){
              res.send(response);
            }
          );
           }else{
             response = {
               "msg" : "Usuario no encontrado."
             };
              res.status(404);
              res.send(response);
           }
         }
       }
    );
  }
);

//Nuevas funcionalidades empiezan a partir de aqui
app.post ('/apitechu/v2/newuser',
  function(req,res){

    var email = req.body.email
    var password = req.body.password
    var query = 'q={"email": "'+ email +'","password" :"'+ password +'"}';

    httpClient = requestJson.createClient(baseMlabURL);

     httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
      if (body.length>0) {
          var response = {
               "msg" : "El usuario ya existe."
          }
            res.status(404);
            res.send(response);
          }else{

            var sortid = 's={"id" :-1}';
            var returnid = 'l=1';

           httpClient.get("user?" + sortid + '&' + returnid + '&' + mLabAPIKey,
           function(err, resMlab, body){
             if (body.length>0) {

               var newId = body[0].id+1;

               var newRegister = {
                "id" : newId,
                "first_name": req.body.first_name,
                "last_name": req.body.last_name,
                "email": req.body.email,
                "country": req.body.country,
                "city": req.body.city,
                "mobile": req.body.mobile,
                "password" : req.body.password
               };

                httpClient.post("user?" + mLabAPIKey ,newRegister,
                  function(errPost,resMlabPost,bodyPost){
                    if (errPost) {
                         var response = {
                            "msg" : "Error alta usuario."
                        }
                        res.send(response);
                        }else{
                          var dni = req.body.dni;
                          var querydni = 'q={"dni": "'+ dni +'"}';

                          httpClient.get("bankaccount?"+ querydni +'&'+ mLabAPIKey,
                            function(err,resMlab,body){
                              if (err){
                                var response={
                                  "msg" :"Error obteniendo cuenta del banco"
                                }
                                res.send(response);
                              }else{
                                var iban = body[0].iban;
                                var balance = body[0].balance;
                                var sortidcuenta = 's={"idcuenta" :-1}';
                                var returnidcuenta = 'l=1';

                                httpClient.get("account?" + sortidcuenta + '&' + returnidcuenta + '&' + mLabAPIKey,
                                  function(err,resMlab,body){

                                    var newCuenta = {
                                     "iban": iban,
                                     "userid" : newId,
                                     "idcuenta" : body[0].idcuenta+1,
                                     "balance": balance
                                    };

                                    if (err){
                                      var response={
                                        "msg":"Error obteniendo cuentas de app"
                                      }
                                      res.send(response);
                                    }else{

                                      httpClient.post("account?"+ mLabAPIKey, newCuenta,
                                        function(errPost,resMlabPost,bodyPost){
                                          if(errPost){
                                            var response ={
                                              "msg":"Error añadiendo nueva cuenta"
                                            }
                                            res.send(response);
                                          }else{
                                            var response ={
                                              "msg":"Nueva cuenta añadida con exito"
                                            }
                                            res.send(response);
                                          }
                                        }
                                      );
                                    }
                                  }
                                );
                              }
                            }
                         );
                       }
                     }
                   );
                 }
               }
             );
           }
         }
       );
     }
  );

   app.get ('/apitechu/v2/cuentas/movimientos/:id',
     function (req,res){

       var sortidmov = 's={"idmov":-1}';
       var idcuenta = req.params.id;
       var query = 'q={"idcuenta" : '+ idcuenta +'}';

       httpClient = requestJson.createClient(baseMlabURL);

       httpClient.get("pruebamov?" + query + '&' + sortidmov +'&'+ mLabAPIKey,
         function(err, resMLab, body) {
           if (err) {
             response = {
               "msg" : "Error obteniendo movimientos de cuenta."
           }
            res.status(500);
           }else{
             if (body.length > 0) {
              response =body;
              }else{
               response = {
                "msg" : "No existen movimientos de cuenta."
             };
              res.status(404);
            }
          }
           res.send(response);
        }
      );
     }
   );

   app.post ('/apitechu/v2/nuevomovimiento',
     function(req,res){

       var movil = req.body.mobile;
       var movTipo = req.body.tipomov;
       var idcuenta = req.body.idcuenta;
       var querymov ='q={"idcuenta" :'+ idcuenta +'}';

       httpClient = requestJson.createClient(baseMlabURL);

       httpClient.get("account?" + querymov + "&" + mLabAPIKey,

       function(err, resMLab, body) {
         if (movil.length<9) {
            var response = {
               "msg" : "Error obteniendo datos de la operacion"
            }
              res.status(404);
              res.send(response);
            }else{
              var movImp = req.body.importemov;
              var prsImp = parseFloat(movImp);
              var cuentaBalance = body[0].balance;
              var cuentaid = body[0].idcuenta;
              var sortidmov = 's={"idmov":-1}';
              var returnidmov = 'l=1';

              httpClient.get("pruebamov?"+ sortidmov +'&'+returnidmov +'&'+ mLabAPIKey,
               function(err, resMlab, body){
                if (movTipo=="ingreso") {

                  var newIngreso = {
                   "idcuenta": cuentaid,
                   "idmov": body[0].idmov+1,
                   "fechamov": hoyfecha,
                   "importemov": prsImp,
                   "tipomov" : "Ingreso Recibido"
                  };

                   httpClient.post("pruebamov?" + mLabAPIKey ,newIngreso,
                     function(errPost,resMlabPost,bodyPost){
                       if (errPost) {
                             response = {
                                "msg" : "Error realizando movimiento."
                           }
                            res.send(response);
                         }else{
                          var query = 'q={"idcuenta":'+idcuenta+'}';

                          var movIngreso = req.body.importemov;
                          var numMov = parseFloat(movIngreso);
                          var totalMov = cuentaBalance+numMov;
                          var putCuenta = '{"$set":{"balance":'+ totalMov +'}}';

                          httpClient.put("account?"+query+'&'+mLabAPIKey, JSON.parse(putCuenta),
                           function(errPut,resMlabPut,bodyPut){
                             var response={
                               "msg" : "ingreso realizado con exito"
                             }
                             res.send(response);
                            }
                          );
                        }
                       }
                     );
                    }if (movTipo=="retiro"){

                         var newRetiro = {
                          "idcuenta": cuentaid,
                          "idmov": body[0].idmov+1,
                          "fechamov": hoyfecha,
                          "importemov":-prsImp,
                          "tipomov" : "Pago Enviado"
                         };

                         httpClient.post("pruebamov?" + mLabAPIKey ,newRetiro,
                           function(errPost,resMlabPost,bodyPost){
                             if(errPost){
                               response ={
                                 "msg":"Error realizando movimiento"
                               }
                               res.send(response);
                             }else{
                               var queryret = 'q={"idcuenta":'+idcuenta+'}';
                               var movRetiro = req.body.importemov;
                               var numRet = parseFloat(movRetiro);
                               var retMov = cuentaBalance-numRet;
                               var retCuenta = '{"$set":{"balance":'+ retMov +'}}';

                                 httpClient.put("account?"+queryret+'&'+mLabAPIKey, JSON.parse(retCuenta),
                                   function(errPut,resMlabPut,bodyPut){
                                     var response={
                                           "msg" : "Retirada realizada con exito"
                                         }
                                           res.send(response);
                                   }
                                 );
                             }
                          }
                       );
                     }
                  }
                );
              }
            }
         );
       }
     );


    app.post ('/apitechu/v2/transferencia',
      function(req,res){

        var iban = req.body.iban;
        var idcuenta = req.body.idcuenta;
        var prsIdCuenta = Number(idcuenta);
        var queryt = 'q={"idcuenta": '+ idcuenta +'}';
         var queryiban = 'q={"iban": "'+ iban +'"}';

        httpClient = requestJson.createClient(baseMlabURL);

        httpClient.get("account?" + queryiban + "&" + queryt + "&" + mLabAPIKey,
        function(err, resMLab, body){
          if (body.length==0){
            var response = {
              "msg" : "IBAN del destino no valido"
            }
            res.status(404);
            res.send(response);
          }else{

            var movTrf = req.body.importemov;
            var prsTrf = parseFloat(movTrf);
            var cuentaBalance = body[0].balance;
            var sortidmov = 's={"idmov":-1}';
            var returnidmov = 'l=1';

            httpClient.get("pruebamov?"+ sortidmov +'&'+returnidmov +'&'+ mLabAPIKey,
             function(err, resMlab, body){
               if (body.length>0){

                 var newTransfer = {
                  "idcuenta": prsIdCuenta,
                  "idmov": body[0].idmov+1,
                  "fechamov": hoyfecha,
                  "importemov": -prsTrf,
                  "tipomov" : "Transferencia Realizada"
                 };

                 httpClient.post("pruebamov?" + mLabAPIKey ,newTransfer,
                   function(errPost,resMlabPost,bodyPost){
                     if (errPost) {
                           var response = {
                              "msg" : "Error grabando transferencia."
                          }
                           res.send(response);
                       }else{
                         if (body.length>0){
                          var querytr = 'q={"idcuenta":'+idcuenta+'}';
                          var movTr = req.body.importemov;
                          var prsMov = parseFloat(movTr);
                          var trMov = cuentaBalance-prsMov;
                          var trCuenta = '{"$set":{"balance":'+ trMov +'}}';

                         httpClient.put("account?"+querytr+'&'+mLabAPIKey, JSON.parse(trCuenta),
                           function(errPut,resMlabPut,bodyPut){
                             if(errPut){
                              var response={
                               "msg" : "Error grabando transferencia"
                             }
                             res.send(response);
                           }else{
                             var idmov =body[0].idmov+2;
                             var prsIdmov = Number(idmov);
                             var iban = req.body.iban;
                             var queryiban = 'q={"iban": "'+ iban +'"}';

                             httpClient.get("account?" + queryiban + "&" + mLabAPIKey,
                             function(err, resMLab, body){
                               if(body==0){
                                 var response ={
                                   "msg" : "Error recibiendo transferencia"
                                 }
                                 res.status(404);
                                 res.send(response);
                               }else{

                               var idcuentadestino = body[0].idcuenta;
                               var prsIdBenef = Number(idcuentadestino);
                                var balanceDestino = body[0].balance;

                                var lastTransfer = {
                                 "idcuenta": prsIdBenef,
                                 "idmov": prsIdmov,
                                 "fechamov": hoyfecha,
                                 "importemov": prsTrf,
                                 "tipomov" : "Transferencia Recibida"
                                };
                                httpClient.post("pruebamov?" + mLabAPIKey ,lastTransfer,
                                  function(errPost,resMlabPost,bodyPost){
                                    if(body==0){
                                      var response={
                                        "msg" : "Error grabando transferencia recibida"
                                        }
                                      res.send(response);
                                    }else{
                                      var movIngreso = req.body.importemov;
                                      var numMov = parseFloat(movIngreso);
                                      var trRec = balanceDestino+numMov;
                                      var recCuenta = '{"$set":{"balance":'+ trRec +'}}';

                                      httpClient.put("account?"+queryiban+'&'+mLabAPIKey, JSON.parse(recCuenta),
                                        function(errPut,resMlabPut,bodyPut){
                                         var response={
                                               "msg" : "Transferencia realizada con exito"
                                             }
                                           res.send(response);
                                        }
                                      );
                                    }
                                  }
                                );
                              }
                            }
                          );
                         }
                       }
                     );
                    }
                   }
                 }
                );
               }
             }
           );
         }
       }
      );
     }
   );
